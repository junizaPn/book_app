import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { SplashScreen, LoginScreen, RegisterScreen, SuccessRegister, HomeTabScreen, BookDetailScreen, MediaScreen} from '../screen';

const Stack = createNativeStackNavigator();

function Route () {
  return (
    <Stack.Navigator initialRouteName='LoginScreen' >
        <Stack.Screen name='SplashScreen' component={SplashScreen} options={{ headerShown: false}}/>
        <Stack.Screen name='LoginScreen' component={LoginScreen} options={{ headerShown: false}} />
        <Stack.Screen name='RegisterScreen' component={RegisterScreen} options={{ headerShown: false}} />
        <Stack.Screen name='SuccessRegister' component={SuccessRegister} options={{ headerShown: false}} />
        <Stack.Screen name='HomeTabScreen' component={HomeTabScreen} options={{ headerShown: false}} />
        <Stack.Screen name='BookDetailScreen' component={BookDetailScreen} options={{ headerShown: false}} />
        <Stack.Screen name='MediaScreen' component={MediaScreen} options={{ headerShown: false}} />
    </Stack.Navigator>
  );
}

export default Route
