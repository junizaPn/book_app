import axios from '../service';
import {
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    LOGIN_ISLOADING,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    REGISTER_ISLOADING,
    GETDATA_SUCCESS,
    GETDATA_FAILED,
    GETDATA_ISLOADING,
    LOGOUT,
    GETDATA_POPULAR,
    GETDATADETAIL_FAILED,
    GETDATADETAIL_ISLOADING,
    GETDATADETAIL_SUCCESS,
    REFRESH_CONTROL
} from '../types';

//action login screen
export const saveLoginsuccess = (data, name) => ({
    type: LOGIN_SUCCESS,
    payload: data, name
})
export const saveLoginfailed = (data) => ({
    type: LOGIN_FAILED,
    payload: data
})
export const saveLoginisloading = (data) => ({
    type: LOGIN_ISLOADING,
    payload: data
})
export const logout = () => ({
    type: LOGOUT,
})
//action login screen
export const login = (email, password, navigation) => async (dispatch) => {
    dispatch({ type: LOGIN_ISLOADING });
    console.log(email)
    try {
        await axios.post('/v1/auth/login', {
            email: email,
            password: password,
        }).then((res) => {
            if (res.data.tokens.access.token) {
                dispatch(saveLoginsuccess(res.data.tokens.access.token, res.data.user.name));
                navigation.navigate('HomeTabScreen');
                alert('Login Berhasil')
            }
        });
    } catch (error) {
        dispatch(saveLoginfailed(error.message))
        alert('username atau password salah');
        console.log(error);
    }
}

//action registration screen
export const saveRegistsuccess = (regis) => ({
    type: REGISTER_SUCCESS,
    payload: regis
})
export const saveRegisfailed = (regis) => ({
    type: REGISTER_FAILED,
    payload: regis
})
export const saveRegisisloading = (regis) => ({
    type: REGISTER_ISLOADING,
    payload: regis
})
export const register = (fullname, email, password, navigation) => async (dispatch) => {
    dispatch({ type: REGISTER_ISLOADING });
    console.log(fullname)
    console.log(email)
    console.log(password)
    try {
        await axios.post('/v1/auth/register', {
            email: email,
            password: password,
            name: fullname
        }).then(res => {
            dispatch(saveRegistsuccess());
            navigation.navigate('SuccessRegister');
            alert('Register Berhasil')
        });
    } catch (error) {
        dispatch(saveRegisisloading(error.message))
        // alert('Register Gagal');
        alert(error.message)
        console.log(error);
    }
}

//action home screen
export const saveDataSuccess = (getData) => ({
    type: GETDATA_SUCCESS,
    payload: getData
})
export const saveDataFailed = (getData) => ({
    type: GETDATA_FAILED,
    payload: getData
})
export const saveDataIsloading = (getData) => ({
    type: GETDATA_ISLOADING,
    payload: getData
})

export const saveDataPopularSuccess = (getData) => ({
    type: GETDATA_POPULAR,
    payload: getData
})
export const refreshSuccess = (getData) =>({
    type: REFRESH_CONTROL,
    payload: getData
})
export const getRecomend = (token, limit) => async (dispatch) => {
    dispatch({ type: GETDATA_ISLOADING });
    try {
        await axios.get('/v1/books', { headers: { Authorization: `Bearer ${token}` }, params: { limit } })
            .then((res) => {
                dispatch(refreshSuccess(false))
                dispatch(saveDataSuccess(res.data.results));
            });
    } catch (error) {
        dispatch(refreshSuccess(false))
        dispatch(saveDataFailed(error.message))
        alert('get recommend error')
        console.log(error);
    }
}

export const getPopular = (token) => async (dispatch) => {
    dispatch({ type: GETDATA_ISLOADING });
    try {
        await axios.get('/v1/books', { headers: { Authorization: `Bearer ${token}` }})
            .then((res) => {
                dispatch(saveDataPopularSuccess(res.data.results));
            });
    } catch (error) {
        dispatch(saveDataFailed(error.message))
        alert(' get popular error')
        console.log(error);
    }
}

//action get detail
export const saveDataDetailSuccess = (getData) => ({
    type: GETDATADETAIL_SUCCESS,
    payload: getData
})
export const saveDataDetailFailed = (getData) => ({
    type: GETDATADETAIL_FAILED,
    payload: getData
})
export const saveDataDetailIsloading = (getData) => ({
    type: GETDATADETAIL_ISLOADING,
    payload: getData
})

export const getDetail = (token, id) => async (dispatch) => {
    dispatch({ type: GETDATADETAIL_ISLOADING });
    console.log('token', token, 'id', id)
    try {
        await axios.get(`/v1/books/${id}`, { headers: { Authorization: `Bearer ${token}` }})
            .then((res) => {
                dispatch(saveDataDetailSuccess(res.data));
                // alert('save data ?')
            });
    } catch (error) {
        dispatch(saveDataDetailFailed(error.message))
        alert(' get detail error')
        console.log(error.message);
    }
}



