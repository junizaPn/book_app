import {
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    LOGIN_ISLOADING,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    REGISTER_ISLOADING,
    GETDATA_SUCCESS,
    GETDATA_FAILED,
    GETDATA_ISLOADING,
    LOGOUT,
    GETDATA_POPULAR,
    GETDATADETAIL_FAILED,
    GETDATADETAIL_SUCCESS,
    GETDATADETAIL_ISLOADING,
    REFRESH_CONTROL
} from '../types';

const initialState = {
    user: null,
    name: null,
    loading: false,
    errorMessage: ''
};
const initialStateHome = {
    recommend: [],
    popular: [],
    loading: false,
    errorMessage: '',
    isRefresh: false
}
const initialStateDetail = {
    getBookDetail: [],
    loading: false,
    errorMessage: ''
}

export const LoginReduce = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                user: action.payload,
                name: action.name,
                loading: false
            };
        case LOGIN_FAILED:
            return {
                ...state,
                errorMessage: action.payload,
                loading: false
            };
        case LOGIN_ISLOADING:
            return {
                ...state,
                loading: true
            };
        case LOGOUT:
            return {
                ...state,
                user: null
            }
        default:
            return state;
    }
}

export const RegisReduce = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_SUCCESS:
            return {
                ...state,
                user: action.payload,
                loading: false
            };
        case REGISTER_FAILED:
            return {
                ...state,
                errorMessage: action.payload,
                loading: false
            };
        case REGISTER_ISLOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}

export const HomeReduce = (state = initialStateHome, action) => {
    switch (action.type) {
        case GETDATA_SUCCESS:
            return {
                ...state,
                recommend: action.payload,
                loading: false
            };
        case GETDATA_FAILED:
            return {
                ...state,
                errorMessage: action.payload,
                loading: false
            };
        case GETDATA_ISLOADING:
            return {
                ...state,
                loading: true
            };
        case GETDATA_POPULAR:
            return {
                ...state,
                popular: action.payload,
                loading: false
            }
        case REFRESH_CONTROL:
            return {
                ...state,
                isRefresh: action.payload
            }
        default:
            return state;
    }
}

export const DetailReduce = (state = initialStateDetail, action) => {
    switch (action.type) {
        case GETDATADETAIL_SUCCESS:
            return {
                ...state,
                getBookDetail: action.payload,
                loading: false
            }
        case GETDATADETAIL_FAILED:
            return {
                ...state,
                errorMessage: action.payload,
                loading: false
            }
        case GETDATADETAIL_ISLOADING:
            return {
                ...state,
                loading: true
            }
        default:
            return state;
    }
}