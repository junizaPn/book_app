import { applyMiddleware, combineReducers, createStore } from "redux";
import ReduxThunk from 'redux-thunk';
import logger from 'redux-logger';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { persistStore, persistReducer } from 'redux-persist';

import {DetailReduce, HomeReduce, LoginReduce, RegisReduce} from "../reducer";

const Reducer = {
    login: LoginReduce,
    register: RegisReduce,
    home: HomeReduce,
    detail: DetailReduce
}
const persistConfig ={
    key: 'root',
    storage: AsyncStorage,
}
const configpersist = persistReducer(persistConfig, combineReducers(Reducer))

export const Store = createStore(configpersist, applyMiddleware(ReduxThunk, logger))
export const Persistor = persistStore(Store)