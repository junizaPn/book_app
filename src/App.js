import React , {useEffect, useState} from 'react';
import { NavigationContainer, } from '@react-navigation/native';
import Route from './route'
import { Provider } from 'react-redux';
import { Store, Persistor } from './redux/store';
import { PersistGate } from 'redux-persist/integration/react';
// import NetInfo from '@react-native-community/netinfo';

function App() {
  // const [isOffline, setOfflineStatus] = useState(false);
  
  // useEffect(() => {
  //   const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
  //     const offline = !(state.isConnected && state.isInternetReachable);
  //     setOfflineStatus(offline);
  //   });
  
  //   return () => removeNetInfoSubscription();
  // }, []);

  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <NavigationContainer>
           <Route/>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  )
}

export default App