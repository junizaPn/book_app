import { Text, View, Image, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native'
import React, {useState, useEffect} from 'react';
import styles from './styles';
import { useDispatch } from 'react-redux';
import { login } from '../../redux/actions';

function Login({ navigation }) {

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

 const dispatch = useDispatch()
  const onSubmit = () => {
    // const data = {email, password, navigation}
   dispatch(login(email, password, navigation))
  }

  const [isLoading, setIsLoading] = React.useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, [])
  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size='large' />
      </View>
    )
  }
  return (
    <View testID="loginView" style={styles.ContainerLoginComponent}>
      <View style={styles.containerHeader}>
        <Image source={require('../../assets/image/logo.png')} style={styles.logome}></Image>
        <Text style={styles.textHeader}>WELCOME BACK</Text>
        <Text style={styles.textTitle}>Login to continue</Text>
      </View>
      <View style={styles.containerInput}>
        <TextInput
          style={styles.textInput}
          onChangeText={text => setEmail(text)}
          value={email}
          placeholder="Email"
          placeholderTextColor="black"
          testID='emailInput'
        />
        <TextInput
          style={styles.textInput}
          onChangeText={text => setPassword(text)}
          value={password}
          placeholder="Password"
          placeholderTextColor="black"
          secureTextEntry={true}
          testID='passwordInput'
        />
        <TouchableOpacity 
        style={styles.buttonLogin} 
        onPress={onSubmit}
        testID='buttonLogin'
        >
          <Text  style={styles.textLogin}>Login</Text>
        </TouchableOpacity>
        <Text style={styles.textAccount}>Don't have an account?</Text>
        <TouchableOpacity testID='buttonRegis' onPress={() => navigation.navigate('RegisterScreen')} >
          <Text style={styles.textRegister}>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
export default Login