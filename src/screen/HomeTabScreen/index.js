import { Text, View, ScrollView, ActivityIndicator, TouchableOpacity, Alert, RefreshControl } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from './styles'
import { useDispatch, useSelector } from 'react-redux'
import { CardRecomend, CardPopular } from '../../component'
import { getRecomend, logout, getPopular, refreshSuccess } from '../../redux/actions'

const HomeTabScreen = ({ navigation }) => {
    const dispatch = useDispatch()

    const token = useSelector(state => state.login.user)
    const isLoading = useSelector(state => state.home.loading)
    const name = useSelector(state => state.login.name)
    const recommend = useSelector(state => state.home.recommend)
    const popular = useSelector(state => state.home.popular)
    const refresh = useSelector(state => state.home.isRefresh)

    const refreshOn = () => {
        dispatch(refreshSuccess(true))
        dispatch(getRecomend(token, 6))
        dispatch(getPopular(token))
    }

    const onLogout = () => {
        Alert.alert('Peringatan', 'anda yakin ingin logout', [{
            text: 'tidak',

        }, {
            text: 'ya', onPress: () => {
                dispatch(logout())
                navigation.replace('SplashScreen')
            }
        }])
    }


    useEffect(() => {
        dispatch(getRecomend(token, 6))
        console.log('recomend', getRecomend)
        dispatch(getPopular(token))
        console.log('popular', getPopular)
    }, [])
    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' />
            </View>
        )
    }
    return (
        <View testID='homeView'>
            <ScrollView refreshControl={
                <RefreshControl refreshing={refresh} onRefresh={() => refreshOn} />
            }
            >
                <View style={styles.containerRcmd}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.txtName}>Good Morning, {name}</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('MediaScreen')} testID='media'>
                            <Text style={styles.media}>Media</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={onLogout} testID='logout'>
                            <Text style={styles.logout}>Logout</Text>
                        </TouchableOpacity>

                    </View>
                    <Text style={styles.txtHeader}>Recommended Book</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <ScrollView testID='scrollRecommend' horizontal showsHorizontalScrollIndicator={false}>
                            {recommend.map(item => (
                                <View
                                    key={item.id}
                                >
                                    <CardRecomend
                                        testID='onpressRecomend'
                                        onPress={() =>
                                            navigation.navigate('BookDetailScreen', { itemId: item.id })
                                        }
                                        source={item.cover_image}
                                    />
                                </View>
                            ))}
                        </ScrollView>
                    </View>
                </View>
                <View style={styles.container}>
                    <ScrollView testID='scrollPopular' vertical>
                        <Text style={styles.txtHeader}>Popular Book</Text>
                        <View style={{ flexDirection: 'column', marginBottom: 5 }}>
                            {popular.map(item => {
                                return (
                                    <View key={item.id}>
                                        <CardPopular
                                            onPress={() =>
                                                navigation.navigate('BookDetailScreen', { itemId: item.id })
                                            }
                                            source={item.cover_image}
                                            write={item.author}
                                            title={item.title}
                                            publish={item.publisher}
                                            rating={item.average_rating}
                                            price={`Rp. ${parseFloat(item.price).toLocaleString('id-ID',)}`}
                                        />
                                    </View>
                                )
                            })}
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        </View >
    )
}

export default HomeTabScreen

