import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    containerRcmd: {

    },
    container: {
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    txtHeader: {
        fontSize: 19,
        fontWeight: 'bold',
        marginHorizontal: 12,
        marginTop: 15,
        color: 'black',
        fontFamily: 'verdana'
    },
    txtName: {
        fontSize: 16,
        marginHorizontal: 12,
        marginTop: 15,
        color: 'black',
        fontFamily: 'verdana'
    },
    logout: {
        marginLeft: 22,
        marginRight: 92,
        color: 'white',
        padding: 10,
        backgroundColor: 'black',
        borderRadius: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: 13,
        flex: 1,
    },
    media: {
        marginLeft: 50,
        color: 'white',
        padding: 10,
        backgroundColor: 'black',
        borderRadius: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: 13,
        flex: 1,
    }
})
export default styles