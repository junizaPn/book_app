import SplashScreen from "./SplashScreen";
import LoginScreen from "./LoginScreen";
import RegisterScreen from "./RegisterScreen";
import SuccessRegister from "./SuccessRegister";
import HomeTabScreen from "./HomeTabScreen";
import BookDetailScreen from "./BookDetailScreen";
import MediaScreen from "./MediaScreen";

export { SplashScreen, LoginScreen, RegisterScreen, SuccessRegister, HomeTabScreen,BookDetailScreen, MediaScreen}