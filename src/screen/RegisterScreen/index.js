import { Text, View, Image, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native';
import React, { useState, useEffect } from 'react';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { useDispatch } from 'react-redux';
import { register } from '../../redux/actions';

function Register({ navigation }) {

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [fullname, setFullname] = useState('')

  const dispatch = useDispatch()
  const onRegis = () => {
    // const data = { email: email, password: password, fullname: fullname, navigation }
    dispatch(register(fullname, email, password, navigation))
  }

  const [isLoading, setIsLoading] = React.useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, [])

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size='large' />
      </View>
    )
  }
  return (
    <View testID='regisView' style={styles.container}>
      <View style={{ marginLeft: 10, marginTop: 5 }}>
        <Icon
          name='md-arrow-back-circle-outline'
          color="white"
          size={45}
          onPress={() => navigation.goBack()}
          testID='buttonBack'
        />
      </View>

      <Image source={require('../../assets/image/logo.png')} style={styles.logome}></Image>
      <View style={styles.boxHeader}>
        <Text style={styles.textTitle}>REGISTER NOW !</Text>
      </View>
      <View>
        <TextInput
          style={styles.textInput}
          onChangeText={text => setFullname(text)}
          value={fullname}
          placeholder="Full Name"
          placeholderTextColor="black"
          testID='inputName'
        />
        <TextInput
          style={styles.textInput}
          onChangeText={text => setEmail(text)}
          value={email}
          placeholder="Email"
          placeholderTextColor="black"
          testID='inputEmail'
        />
        <TextInput
          style={styles.textInput}
          onChangeText={text => setPassword(text)}
          value={password}
          placeholder="Password"
          placeholderTextColor="black"
          secureTextEntry={true}
          testID='inputPassword'
        />
        <TouchableOpacity style={styles.buttonRegister} onPress={onRegis} testID='buttonRegisScreen'>
          <Text style={styles.textRegister}>Register</Text>
        </TouchableOpacity>
        <Text style={styles.textAccount}>Already have an account ?</Text>
        <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')} testID='buttonLoginRegis'>
          <Text style={styles.textLogin}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
export default Register