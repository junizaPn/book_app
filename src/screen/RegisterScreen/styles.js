import { StyleSheet} from 'react-native'
const styles = StyleSheet.create({
    container:{
        backgroundColor: '#4d6000',
        width:415,
        height: 260,
    },
    boxHeader:{
        backgroundColor: '#7ed957',
        width: 350,
        height: 70,
        alignSelf: 'center',
        marginVertical: 5,
        borderRadius: 15,    
    },
    logome:{
        width: 170,
        height: 170,
        alignSelf: 'center',
    },
    textTitle:{
        marginHorizontal: 15,
        marginVertical: 17,
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 25,
        color: 'white'
    },
    textInput:{
        backgroundColor: 'white',
        borderColor: 'white',
        borderWidth: 3,
        borderRadius: 12,
        padding: 14,
        marginVertical: 15,
        marginHorizontal: 15,
        fontSize: 18
        
    },
    buttonRegister:{
        backgroundColor: '#c9e265',
        borderColor: '#c9e265',
        borderWidth: 3,
        borderRadius: 25,
        padding: 6,
        marginVertical: 15,
        marginHorizontal: 120,
    },
    textRegister:{
        color: 'black',
        fontSize: 20,
        alignSelf: 'center',
    },
    textLogin:{
        color: 'black',
        fontWeight: 'bold',
        fontSize: 25,
        alignSelf: 'center',
        marginTop: 12
    },
    textAccount:{
        fontSize: 18,
        alignSelf: 'center',
    },
})
export default styles;