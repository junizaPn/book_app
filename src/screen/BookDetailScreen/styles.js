import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container:{
        width:420,
        height: 55,
        backgroundColor: '#4d6000',
    },
    imageBg:{
        height: 460,
    },
    boxSynop:{
        marginLeft: 15,
        marginTop: 10,
        marginRight: 5
    },
    txtTitle:{
        fontWeight: 'bold',
        fontSize: 18,
        color: 'black',
    },
    txtDes:{
        color: 'black'
    }
})
export default styles