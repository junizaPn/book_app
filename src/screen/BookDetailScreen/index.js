import { Text, View, ImageBackground, ActivityIndicator, ScrollView, RefreshControl, TouchableOpacity, Share } from 'react-native'
import React, { useEffect } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { CardImageDetail, CardDetail } from '../../component'
import { useDispatch, useSelector } from 'react-redux'
import { getDetail, refreshSuccess } from '../../redux/actions';
import { configure, buatChannel, kirimNotifikasi } from '../../utils';

const BookDetailScreen = ({ route, navigation }) => {
    const onShare = async () => {
        try {
            const result = await Share.share({
                message: 'share you favorite book',
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType 
                } else {
                    // shared 
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed 
            }
        } catch (error) {
            alert(error.message);
        }
    };

    const { itemId } = route.params
    const dispatch = useDispatch()

    const token = useSelector(state => state.login.user)
    const detail = useSelector(state => state.detail.getBookDetail)
    const isLoading = useSelector(state => state.detail.loading)
    const refresh = useSelector(state => state.home.isRefresh)

    const klikTombol = () => {
        configure();
        buatChannel("1");
        kirimNotifikasi("1", "Aplikasi Book Online", "Your Favorite Book");
    }

    const refreshOn = () => {
        dispatch(refreshSuccess(true))
        dispatch(getRecomend(token, 6))
        dispatch(getPopular(token))
    }

    useEffect(() => {
        dispatch(getDetail(token, itemId))
        console.log('detail', detail)
    }, [])
    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' />
            </View>
        )
    }
    return (
        <View>
            <ScrollView refreshControl={
                <RefreshControl refreshing={refresh} onRefresh={() => refreshOn} />
            }
            >
                <ImageBackground source={require('../../assets/image/bg.png')} style={styles.imageBg}>
                    <View style={styles.container}>
                        <View style={{ flexDirection: 'row', marginHorizontal: 10, marginVertical: 10, }}>
                            <Icon
                                name='md-arrow-back-circle-outline'
                                color="white"
                                size={35}
                                onPress={() => navigation.goBack()}
                            />
                            <View style={{ flexDirection: 'row', }}>
                                <TouchableOpacity onPress={klikTombol}>
                                    <Icon
                                        style={{ marginLeft: 265 }}
                                        name='md-heart-circle-outline'
                                        color="white"
                                        size={35}
                                    />
                                </TouchableOpacity >
                                <TouchableOpacity onPress={onShare}>
                                    <Icon
                                        style={{ marginHorizontal: 10 }}
                                        name='md-share-social-outline'
                                        color="white"
                                        size={35}
                                    />
                                </TouchableOpacity>

                            </View>
                        </View>
                        <View>
                            <CardImageDetail
                                source={detail.cover_image}
                                title={detail.title}
                                author={detail.author}
                                publish={detail.publisher}
                            />
                        </View>

                    </View>
                </ImageBackground>
                <CardDetail
                    rating={detail.average_rating}
                    sale={detail.total_sale}
                    price={`Rp. ${parseFloat(detail.price).toLocaleString('id-ID',)}`}
                />
                <View style={styles.boxSynop}>
                    <Text style={styles.txtTitle}>Synopsis</Text>
                    <Text style={styles.txtDes}>{detail.synopsis}</Text>
                </View>
            </ScrollView>
        </View>
    )
}
export default BookDetailScreen