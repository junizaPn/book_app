import { Button, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import Pdf from 'react-native-pdf';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/Ionicons';

const MediaScreen = ({ navigation }) => {
    return (
        <View>
            <TouchableOpacity>
                <Icon
                    name='md-arrow-back-circle-outline'
                    color="black"
                    size={35}
                    onPress={() => navigation.goBack()}
                />
            </TouchableOpacity>

            <View style={{ alignItems: 'center', marginTop: 10, justifyContent: 'flex-start' }}>
                <Text style={{ fontWeight: 'bold' }}>Media Handling Video</Text>
                <Video
                    source={{ uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4' }}
                    style={{ width: 300, height: 300, alignItems: 'center', marginTop: 15, }}
                    fullscreen={false}
                    controls={true}
                    poster="https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/English_Cocker_Spaniel_4.jpg/800px-English_Cocker_Spaniel_4.jpg"
                />
            </View>
            <View style={{ alignItems: 'center', justifyContent: 'flex-end', marginTop: 15 }}>
                <Text style={{ fontWeight: 'bold' }}>Document PDF</Text>
                <Button title='Open PDF Document' />

                <Pdf
                    source={{ uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf' }}
                    style={{ height: 400, width: 400, marginTop: 5 }}
                />
            </View>
        </View>

    )
}

export default MediaScreen

const styles = StyleSheet.create({})