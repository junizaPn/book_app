import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#4d6000',
    },
    textRegis: {
        color: 'white',
        fontSize: 25,
        fontWeight: 'bold',
        alignSelf: 'center',
        marginTop: 100
    },
    textSent: {
        color: 'white',
        fontSize: 25,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 40,
        textAlign: 'center',
        marginHorizontal: 15,
    },
    buttonLogin: {
        backgroundColor: '#c9e265',
        borderColor: '#c9e265',
        borderWidth: 3,
        borderRadius: 7,
        padding: 6,
        marginVertical: 150,
        marginHorizontal: 15,
    },
    textLogin: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 25,
        alignSelf: 'center',
    }
})
export default styles