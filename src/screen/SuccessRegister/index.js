import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import styles from './styles'
import Icon from 'react-native-vector-icons/Ionicons';

const SuccessRegister = ({ navigation }) => {
    return (
        <View testID='successRegis' style={styles.container}>
            <Text style={styles.textRegis}>Registration Completed!</Text>
            <View style={{ marginTop: 120, marginHorizontal: 5, alignItems: 'center' }}>
                <Icon
                    name='checkmark-circle'
                    color="#d9d9d9"
                    size={150}
                    onPress={() => navigation.goBack()}
                />
            </View>
            <Text style={styles.textSent}>We sent email verification to your email</Text>
            <TouchableOpacity style={styles.buttonLogin} onPress={() => navigation.navigate('LoginScreen')} testID='backLogin'>
                <Text style={styles.textLogin}>Back to Login</Text>
            </TouchableOpacity>
        </View>
    )
}

export default SuccessRegister