import { ImageBackground, StyleSheet, View} from 'react-native';
import React, {useEffect} from 'react';
import { useSelector } from 'react-redux';
import { SplashImage } from '../../assets';
// import { styles } from '../styles';

function SplashScreen({ navigation }) {

    const token = useSelector(state => state.login.user)

    useEffect(() => {
        setTimeout(() => {
            if(token){
                navigation.replace('HomeTabScreen');
            }else{
                navigation.replace('LoginScreen');
            }
        }, 3000);
    }, [navigation]);

    return (
        <View testID="splashView">
            <ImageBackground source={require('../../assets/image/desainSplash.png')} style={styles.background}></ImageBackground>
        </View>
        
    )
}

export default SplashScreen
const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },
})