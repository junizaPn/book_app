import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container:{
        width: 420,
        height: 420,
        justifyContent: 'center',
        alignContent: 'center',
        marginTop: 30,
    },
    image:{
        height: 250,
        width: 180,
        borderRadius: 10,
        alignSelf:'center'
    },
    txtDes1:{
        fontSize: 20,
        marginBottom: 6,
        fontWeight: 'bold',
        color: 'black',
        textAlign: 'center',
        alignSelf:'center'
    },
    txtDes2:{
        fontSize: 16,
        marginBottom: 6,
        color: 'black',
        alignSelf:'center'
    },
    txtDes3:{
        fontSize: 16,
        marginBottom: 6,
        color: 'black',
        alignSelf:'center',
        textAlign: 'center',
    },
    txtPrice:{
        marginRight: 5,
        color: 'black',
        fontSize: 17,
        alignSelf: 'center',
        marginTop: 2,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 5,
        marginLeft: 18,
    },
    txtrate:{
        color: 'black',
        fontSize: 17,
        alignSelf: 'center',
        marginLeft: 10,
    },
    txtsale:{
        color: 'black',
        fontSize: 17,
        alignSelf: 'center',
    },
})
export default styles;