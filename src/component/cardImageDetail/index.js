import { Text, View, Image } from 'react-native'
import React from 'react'
import styles from './styles'

const CardImageDetail = (props) => {
    return (
        <View>
            <View style={styles.container}>
                <View>
                    <View style={{ flexDirection: 'column' }}>
                        <Image source={{ uri: props.source }} style={styles.image} />
                        <Text style={styles.txtDes1}>
                            {props.title}
                        </Text>
                        <Text style={styles.txtDes2}>
                            {props.author}
                        </Text>
                        <Text style={styles.txtDes3}>
                            {props.publish}
                        </Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default CardImageDetail

