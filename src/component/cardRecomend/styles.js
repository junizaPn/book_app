import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
    },
    image:{
        marginLeft: 10,
        marginRight: 10,
        marginTop: 15,
        borderRadius: 5,
        width: 140,
        height: 220,
        marginBottom: 10,
    },
    txtTitle:{
        fontWeight: 'bold',
        color: 'black',
        marginBottom: 5,
        textAlign: 'center',
        marginBottom: 5
    },
    txtWrite:{
        color: 'white',
        marginBottom: 5,
        backgroundColor: '#4d6000',
        borderRadius: 5,
        padding: 7,
        fontSize: 12,
        marginBottom: 20
    },
    containerDes:{
        width: 130,
        marginLeft: 12,
        marginTop: 13,
        alignItems: 'center'
    },
    price:{
        fontWeight: 'bold',
        color: 'black',
        marginBottom: 5
    }
})
export default styles