import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import styles from './styles';

function CardRecomend(props, navigation) {
    return (
        <View >
            <View style={styles.container}>
                <TouchableOpacity onPress={props.onPress} >
                    <Image source={{uri: props.source}} style={styles.image} />
                </TouchableOpacity>
            </View>
            {/* <View style={styles.containerDes}>
                <Text style={styles.txtTitle}>{props.title}</Text>
                <Text style={styles.txtWrite}>{props.write}</Text>
            </View> */}
        </View>
    )
}

export default CardRecomend

