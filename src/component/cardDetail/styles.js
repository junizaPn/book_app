import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container:{
        flex: 1,
        height: 56,
        backgroundColor: '#CD853F',
        borderRadius: 5,
        alignSelf: 'center',
        flexDirection: 'row',
    },
    txtPrice:{
        marginRight: 5,
        color: 'black',
        fontSize: 17,
        alignSelf: 'center',
        marginTop: 2,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 5,
        marginLeft: 15,
    },
    txtrate:{
        marginLeft: 25,
        color: 'black',
        fontSize: 17,
        alignSelf: 'center',
        marginLeft: 1,
        flexWrap: 'wrap'
    },
    txtsale:{
        marginLeft: 25,
        color: 'black',
        fontSize: 17,
        alignSelf: 'center',
    },
})
export default styles;