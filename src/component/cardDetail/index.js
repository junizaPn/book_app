import { Text, View, Image } from 'react-native'
import React from 'react'
import styles from './styles'

const CardDetail = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.txtrate}>Rate: {props.rating}</Text>
            <Text style={styles.txtsale}>Total Sale: {props.sale}</Text>
            <Text style={styles.txtPrice}>Buy: {props.price}</Text>
        </View>
    )
}

export default CardDetail

