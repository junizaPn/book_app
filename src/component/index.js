import CardRecomend from "./cardRecomend";
import CardPopular from "./cardPopular";
import CardImageDetail from "./cardImageDetail";
import CardDetail from "./cardDetail";

export {CardRecomend, CardPopular,CardImageDetail, CardDetail}