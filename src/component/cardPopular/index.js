import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import styles from './styles';

function CardPopular(props) {
    return (
        <View style={{flexDirection: 'row'}}>
            <View style={styles.container}>
                <TouchableOpacity onPress={props.onPress} testID='onpressPopular'>
                    <Image source={{uri: props.source}} style={styles.image} />
                </TouchableOpacity>
            </View>
            <View style={styles.containerDes}>
                <Text style={styles.txtWrite}>{props.write}</Text>
                <Text style={styles.txtTitle}>{props.title}</Text>
                <Text style={styles.txtPrice}>{props.publish}</Text>
                <View style={{flexDirection: 'row'}}>
                    <Text style={styles.txtPrice}>{props.price}</Text>
                    <Text style={styles.txtPrice}>Rate: {props.rating}</Text>
                </View>
            </View>
        </View>
    )
}

export default CardPopular

