import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'space-evenly'
    },
    image:{
        marginLeft: 25,
        marginRight: 20,
        marginTop: 15,
        borderRadius: 5,
        width: 135,
        height: 200,
    },
    txtTitle:{
        fontWeight: 'bold',
        color: 'black',
        marginBottom: 5,
        marginRight: 30
    },
    txtWrite:{
        color: 'white',
        marginBottom: 5,
        backgroundColor: '#4d6000',
        borderRadius: 5,
        padding: 10,
        fontSize: 12,
        width: 120,
        textAlign: 'center'
    },
    containerDes:{
        width: 160,
        marginLeft: 25,
        marginRight: 30,
        marginTop: 13,
    },
    price:{
        fontWeight: 'bold',
        color: 'black',
        marginBottom: 5
    },
    txtPrice:{
        marginRight: 10,
        color: 'black',
    }
})
export default styles