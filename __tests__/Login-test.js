import 'react-native';
import React from 'react';
import LoginScreen from '../src/screen/LoginScreen'
// import renderer from 'react-test-renderer';
import renderer from 'react-test-renderer'

describe('LoginScreen', () => {
    it('should show hello world', () => {
        const textHelloWorld = 'Hello World!';
        const notFound = 'Not Found Text';

        const { toJSON, getByText, queryByText } = renderer(<LoginScreen />);

        expect(foundHelloWorldTextElement.props.children).toEqual(textHelloWorld);
        expect(notFound).toBeNull();
        expect(toJSON()).toMatchSnapshot();
    });
    it('should find the button', () => {
        const testIdName = 'pressLogin';
    
        const {getByTestId} = renderer(<LoginScreen />);
    
        const foundButton = getByTestId(testIdName);
    
        expect(foundButton).toBeTruthy();
      });
})