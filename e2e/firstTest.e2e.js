describe('TestDetoxLogin', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    // await device.reloadReactNative();
  });

  it('should have login screen', async () => {
    await expect(element(by.id('loginView'))).toBeVisible();
  });

  it('should fill register button form', async () => {
    await element(by.id('buttonRegis')).tap();
  });

  it('should have register screen', async () => {
    await expect(element(by.id('regisView'))).toBeVisible();
  });

  it('should fill register form', async () => {
    await element(by.id('inputName')).replaceText('Juniza');
    await element(by.id('inputEmail')).replaceText('nugraheni12@gmail.com');
    await element(by.id('inputPassword')).replaceText('nugraheni2022');
    await element(by.id('buttonRegisScreen')).tap();
  });
  it('should have success register screen', async () => {
    await waitFor(element(by.id('successRegis'))).toBeVisible().withTimeout(5000)
    await expect(element(by.id('successRegis'))).toBeVisible();
  });
  it('should fill login button on register screen form', async () => {
    await element(by.id('backLogin')).tap();
  });

  //testing login form after register screen and form testing
  it('should fill login form', async () => {
    await element(by.id('emailInput')).typeText('nugraheni12@gmail.com');
    await element(by.id('passwordInput')).typeText('nugraheni2022\n');
    await element(by.id('buttonLogin')).tap();
  });

  //testing home screen after login and register screen
  it('should have home tab screen', async () => {
    await expect(element(by.id('homeView'))).toBeVisible();
  })
  it('should enable scroll horizontal', async () => {
    await element(by.id('scrollRecommend')).swipe('left');
  })
  it('should enable scroll vertical', async () => {
    await element(by.id('scrollPopular')).scrollTo('bottom');
  })
  it('should fill media button from home tab screen', async () => {
    await element(by.id('media')).tap();
  });
  // it('should enable swipe vertical', async () => {
  //   await element(by.id('scrollPopular')).swipe('bottom');
  // })
  // it('should enable props image to detail screen', async () => {
  //   await element(by.id('onpressRecomend')).tap();
  // })
});
